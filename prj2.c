#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include<semaphore.h>

#define length 10

int createmailbox(pid) { //we would need the caller's pid as the mailbox's name
	
	sem_t semaphore // want to create a unique semaphore valued 1 for every mailbox. don't know how
	struct mailbox *pid;
	pid = (struct mailbox *)malloc (sizeof(struct mailbox));
	if (pid == NULL) exit(1);
	pid->buffer = NULL;
	pid->permission = NULL;
	pid->length = length; 
}

void removemailbox(pid) {

	free(pid);
	pid = NULL;

}



deposit(pid_t src, pid_t dest, struct message) {
	//check permission	
	if (sizeof(struct mailbox dest) == 0) createmailbox(dest);
	int i;
	
	//sem_t s;
	//sem_init (&s,0,1);	//set semaphore to one. only one process can access to mailbox
	sem_wait(&s);	
	for (i=0;i<length;i++) {	//we are appending msgs from behind
		if (dest->buffer[i] == NULL)
			dest->buffer[i] = message;
			sem_post(&s);
			break; 
	}
	if (i==length) {
		sem_post(&s);
	//reply an error msg back to the calling process. don't know how.
	}

retrieve(struct mailbox src) {
	message local;
	sem_wait(&s);	
	if (src.buffer[0] != NULL) {
		local = src->buffer[0];
		for (int i=1; i<length; i++) {
			src->buffer[i-1] = src->buffer[i];
		}
		sem_post(&s);
		break;	
	} 
	else {
		//reply with an error msg
	}
}
