#include <lib.h>
#include <unistd.h>

PUBLIC int deposit(pid_t dest, void* data, size_t length) {
	message m;
	m.m1_i1 = dest;
	m.m1_i2 = length;
	m.m1_p1 = data;
	return (_syscall(PM_PROC_NR, 49, &m));
}

PUBLIC int retrieve(void *data, size_t length) {
	message m;
	m.m1_i1 = length;
        m.m1_p1 = data;
	return (_syscall(PM_PROC_NR, 50, &m));
}

PUBLIC int createmailbox() {
	message m;
	return (_syscall(PM_PROC_NR, 69, &m));
}

PUBLIC int removemailbox() {
	message m;
	return (_syscall(PM_PROC_NR, 70, &m));
}

PUBLIC int mailboxblacklist(pid_t p) {
        message m;
        m.m1_i1 = p;
        return (_syscall(PM_PROC_NR, 57, &m));
}

PUBLIC int mailboxallow(pid_t p) {
        message m;
        m.m1_i1 = p;
        return (_syscall(PM_PROC_NR, 58, &m));
}
