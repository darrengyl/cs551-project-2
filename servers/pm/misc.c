/* Miscellaneous system calls.				Author: Kees J. Bot
 *								31 Mar 2000
 * The entry points into this file are:
 *   do_reboot: kill all processes, then reboot system
 *   do_svrctl: process manager control
 *   do_getsysinfo: request copy of PM data structure  (Jorrit N. Herder)
 *   do_getprocnr: lookup process slot number  (Jorrit N. Herder)
 *   do_memalloc: allocate a chunk of memory  (Jorrit N. Herder)
 *   do_memfree: deallocate a chunk of memory  (Jorrit N. Herder)
 *   do_getsetpriority: get/set process priority
 */

#include "pm.h"
#include <minix/callnr.h>
#include <signal.h>
#include <sys/svrctl.h>
#include <sys/resource.h>
#include <minix/com.h>
#include <string.h>
#include "mproc.h"
#include "param.h"

/*===========================================================================*
*				do_deposit
*============================================================================*/

PUBLIC int do_deposit() {
        struct mproc *dest = &mproc[proc_from_pid(m_in.m1_i1)];
        size_t length;
        permission_t *p;
        int x;

        for(x=0; x < 100; x++) {
                p = &dest->msg_blacklist[x];
                if(p->active && (p->process == who)) {
                        return EACCES;
                }
        }

        if(dest->msg_queue_size >= 0 && dest->msg_queue_size < 10) {
                message_t *empty = dest->msg_first + dest->msg_queue_size;
                if(empty >= (dest->msg)+10) {
                        empty -= 10;
                }

                length = m_in.m1_i2;
                if(length > 48) {
                        /* message content too large to fit in our buffers */
                        return EFBIG;
                } else {
		        sys_datacopy(who, (vir_bytes) m_in.m1_p1, PM_PID, (vir_bytes) empty->content, (phys_bytes) length);
                        empty->length = length;
                        dest->msg_queue_size++;
                        return 0;
                }
        } else if(dest->msg_queue_size < 10){
                /* The mailbox is full */
                return ENOMEM;
        } else {
                /* The mailbox is not set up */
                return ENOENT;
        }
}

/*===========================================================================*
*				do_retrieve
*============================================================================*/
PUBLIC int do_retrieve() {
        message_t *msg;
        if( mp->msg_queue_size < 0 ) {
                /* The mailbox is not set up */
                return ENOENT;
        } else if(mp->msg_queue_size == 0) {
                return 0;
        } else {
                msg = mp->msg_first;
                if(msg->length > m_in.m1_i1) {
                        /* Client side buffer too small. No place to copy the message to */
                        return ENOMEM;
                } else {
                        sys_datacopy(PM_PID, (vir_bytes) msg->content, who, (vir_bytes) m_in.m1_p1, (phys_bytes) msg->length);
                        mp->msg_first ++;
                        if(mp->msg_first == (mp->msg)+10) {
                                mp->msg_first = mp->msg;
                        }
                        mp->msg_queue_size --;
                        return msg->length;
                }
        }
}

/*===========================================================================*
*				do_createmailbox
*============================================================================*/
PUBLIC int do_createmailbox() { /*we would need the caller's pid as the mailbox's name*/
        int x;
        mp->msg_queue_size = 0;
        mp->msg_first = mp->msg;

        for(x=0; x < 100; x ++) {
                mp->msg_blacklist[x].active = 0;
        }

        return 0;
}


/*===========================================================================*
*				do_removemailbox
*============================================================================*/
PUBLIC int do_removemailbox() {	
        mp->msg_queue_size = -1;
        return 0;
}

/*===========================================================================*
 *                              do_mailboxblacklist                          *
 *===========================================================================*/
PUBLIC int do_mailboxblacklist() {
        pid_t bad = m_in.m1_i1;
        int x;
        permission_t *p;
        
        for(x=0; x < 100; x ++) {
                p = &mp->msg_blacklist[x];
                if(!p->active) {
                        p->active = 1;
                        p->process = bad;
                        return 0;
                }
        }
        return ENOMEM;
}

/*===========================================================================*
 *                              do_mailboxallow                              *
 *===========================================================================*/
PUBLIC int do_mailboxallow() {
        pid_t bad = m_in.m1_i1;
        int x;
        permission_t *p;

        for(x=0; x < 100; x ++) {
                p = &mp->msg_blacklist[x];
                if(p->active && (p->process == bad)) {
                        p->active = 0;
                        return 0;
                }
        }
        return ESRCH;
}

/*===========================================================================*
 *				do_allocmem				     *
 *===========================================================================*/
PUBLIC int do_allocmem()
{
  vir_clicks mem_clicks;
  phys_clicks mem_base;

  mem_clicks = (m_in.memsize + CLICK_SIZE -1 ) >> CLICK_SHIFT;
  mem_base = alloc_mem(mem_clicks);
  if (mem_base == NO_MEM) return(ENOMEM);
  mp->mp_reply.membase =  (phys_bytes) (mem_base << CLICK_SHIFT);
  return(OK);
}

/*===========================================================================*
 *				do_freemem				     *
 *===========================================================================*/
PUBLIC int do_freemem()
{
  vir_clicks mem_clicks;
  phys_clicks mem_base;

  mem_clicks = (m_in.memsize + CLICK_SIZE -1 ) >> CLICK_SHIFT;
  mem_base = (m_in.membase + CLICK_SIZE -1 ) >> CLICK_SHIFT;
  free_mem(mem_base, mem_clicks);
  return(OK);
}

/*===========================================================================*
 *				do_getsysinfo			       	     *
 *===========================================================================*/
PUBLIC int do_getsysinfo()
{
  struct mproc *proc_addr;
  vir_bytes src_addr, dst_addr;
  struct kinfo kinfo;
  size_t len;
  int s;

  switch(m_in.info_what) {
  case SI_KINFO:			/* kernel info is obtained via PM */
        sys_getkinfo(&kinfo);
        src_addr = (vir_bytes) &kinfo;
        len = sizeof(struct kinfo);
        break;
  case SI_PROC_ADDR:			/* get address of PM process table */
  	proc_addr = &mproc[0];
  	src_addr = (vir_bytes) &proc_addr;
  	len = sizeof(struct mproc *);
  	break; 
  case SI_PROC_TAB:			/* copy entire process table */
        src_addr = (vir_bytes) mproc;
        len = sizeof(struct mproc) * NR_PROCS;
        break;
  default:
  	return(EINVAL);
  }

  dst_addr = (vir_bytes) m_in.info_where;
  if (OK != (s=sys_datacopy(SELF, src_addr, who, dst_addr, len)))
  	return(s);
  return(OK);
}

/*===========================================================================*
 *				do_getprocnr			             *
 *===========================================================================*/
PUBLIC int do_getprocnr()
{
  register struct mproc *rmp;
  static char search_key[PROC_NAME_LEN+1];
  int key_len;
  int s;

  if (m_in.pid >= 0) {				/* lookup process by pid */
  	for (rmp = &mproc[0]; rmp < &mproc[NR_PROCS]; rmp++) {
		if ((rmp->mp_flags & IN_USE) && (rmp->mp_pid==m_in.pid)) {
  			mp->mp_reply.procnr = (int) (rmp - mproc);
  			return(OK);
		} 
	}
  	return(ESRCH);			
  } else if (m_in.namelen > 0) {		/* lookup process by name */
  	key_len = MIN(m_in.namelen, PROC_NAME_LEN);
 	if (OK != (s=sys_datacopy(who, (vir_bytes) m_in.addr, 
 			SELF, (vir_bytes) search_key, key_len))) 
 		return(s);
 	search_key[key_len] = '\0';	/* terminate for safety */
  	for (rmp = &mproc[0]; rmp < &mproc[NR_PROCS]; rmp++) {
		if ((rmp->mp_flags & IN_USE) && 
			strncmp(rmp->mp_name, search_key, key_len)==0) {
  			mp->mp_reply.procnr = (int) (rmp - mproc);
  			return(OK);
		} 
	}
  	return(ESRCH);			
  } else {				/* return own process number */
  	mp->mp_reply.procnr = who;
  }
  return(OK);
}

/*===========================================================================*
 *				do_reboot				     *
 *===========================================================================*/
#define REBOOT_CODE 	"delay; boot"
PUBLIC int do_reboot()
{
  char monitor_code[32*sizeof(char *)];		
  int code_len;
  int abort_flag;

  if (mp->mp_effuid != SUPER_USER) return(EPERM);

  switch (m_in.reboot_flag) {
  case RBT_HALT:
  case RBT_PANIC:
  case RBT_RESET:
	abort_flag = m_in.reboot_flag;
	break;
  case RBT_REBOOT:
	code_len = strlen(REBOOT_CODE) + 1;
	strncpy(monitor_code, REBOOT_CODE, code_len);        
	abort_flag = RBT_MONITOR;
	break;
  case RBT_MONITOR:
	code_len = m_in.reboot_strlen + 1;
	if (code_len > sizeof(monitor_code)) return(EINVAL);
	if (sys_datacopy(who, (vir_bytes) m_in.reboot_code,
		PM_PROC_NR, (vir_bytes) monitor_code,
		(phys_bytes) (code_len)) != OK) return(EFAULT);
	if (monitor_code[code_len-1] != 0) return(EINVAL);
	abort_flag = RBT_MONITOR;
	break;
  default:
	return(EINVAL);
  }

  check_sig(-1, SIGKILL); 		/* kill all processes except init */
  tell_fs(REBOOT,0,0,0);		/* tell FS to prepare for shutdown */

  /* Ask the kernel to abort. All system services, including the PM, will 
   * get a HARD_STOP notification. Await the notification in the main loop.
   */
  sys_abort(abort_flag, PM_PROC_NR, monitor_code, code_len);
  return(SUSPEND);			/* don't reply to killed process */
}

/*===========================================================================*
 *				do_getsetpriority			     *
 *===========================================================================*/
PUBLIC int do_getsetpriority()
{
	int arg_which, arg_who, arg_pri;
	int rmp_nr;
	struct mproc *rmp;

	arg_which = m_in.m1_i1;
	arg_who = m_in.m1_i2;
	arg_pri = m_in.m1_i3;	/* for SETPRIORITY */

	/* Code common to GETPRIORITY and SETPRIORITY. */

	/* Only support PRIO_PROCESS for now. */
	if (arg_which != PRIO_PROCESS)
		return(EINVAL);

	if (arg_who == 0)
		rmp_nr = who;
	else
		if ((rmp_nr = proc_from_pid(arg_who)) < 0)
			return(ESRCH);

	rmp = &mproc[rmp_nr];

	if (mp->mp_effuid != SUPER_USER &&
	   mp->mp_effuid != rmp->mp_effuid && mp->mp_effuid != rmp->mp_realuid)
		return EPERM;

	/* If GET, that's it. */
	if (call_nr == GETPRIORITY) {
		return(rmp->mp_nice - PRIO_MIN);
	}

	/* Only root is allowed to reduce the nice level. */
	if (rmp->mp_nice > arg_pri && mp->mp_effuid != SUPER_USER)
		return(EACCES);
	
	/* We're SET, and it's allowed. Do it and tell kernel. */
	rmp->mp_nice = arg_pri;
	return sys_nice(rmp_nr, arg_pri);
}

/*===========================================================================*
 *				do_svrctl				     *
 *===========================================================================*/
PUBLIC int do_svrctl()
{
  int s, req;
  vir_bytes ptr;
#define MAX_LOCAL_PARAMS 2
  static struct {
  	char name[30];
  	char value[30];
  } local_param_overrides[MAX_LOCAL_PARAMS];
  static int local_params = 0;

  req = m_in.svrctl_req;
  ptr = (vir_bytes) m_in.svrctl_argp;

  /* Is the request indeed for the MM? */
  if (((req >> 8) & 0xFF) != 'M') return(EINVAL);

  /* Control operations local to the PM. */
  switch(req) {
  case MMSETPARAM:
  case MMGETPARAM: {
      struct sysgetenv sysgetenv;
      char search_key[64];
      char *val_start;
      size_t val_len;
      size_t copy_len;

      /* Copy sysgetenv structure to PM. */
      if (sys_datacopy(who, ptr, SELF, (vir_bytes) &sysgetenv, 
              sizeof(sysgetenv)) != OK) return(EFAULT);  

      /* Set a param override? */
      if (req == MMSETPARAM) {
  	if (local_params >= MAX_LOCAL_PARAMS) return ENOSPC;
  	if (sysgetenv.keylen <= 0
  	 || sysgetenv.keylen >=
  	 	 sizeof(local_param_overrides[local_params].name)
  	 || sysgetenv.vallen <= 0
  	 || sysgetenv.vallen >=
  	 	 sizeof(local_param_overrides[local_params].value))
  		return EINVAL;
  		
          if ((s = sys_datacopy(who, (vir_bytes) sysgetenv.key,
            SELF, (vir_bytes) local_param_overrides[local_params].name,
               sysgetenv.keylen)) != OK)
               	return s;
          if ((s = sys_datacopy(who, (vir_bytes) sysgetenv.val,
            SELF, (vir_bytes) local_param_overrides[local_params].value,
              sysgetenv.keylen)) != OK)
               	return s;
            local_param_overrides[local_params].name[sysgetenv.keylen] = '\0';
            local_param_overrides[local_params].value[sysgetenv.vallen] = '\0';

  	local_params++;

  	return OK;
      }

      if (sysgetenv.keylen == 0) {	/* copy all parameters */
          val_start = monitor_params;
          val_len = sizeof(monitor_params);
      } 
      else {				/* lookup value for key */
      	  int p;
          /* Try to get a copy of the requested key. */
          if (sysgetenv.keylen > sizeof(search_key)) return(EINVAL);
          if ((s = sys_datacopy(who, (vir_bytes) sysgetenv.key,
                  SELF, (vir_bytes) search_key, sysgetenv.keylen)) != OK)
              return(s);

          /* Make sure key is null-terminated and lookup value.
           * First check local overrides.
           */
          search_key[sysgetenv.keylen-1]= '\0';
          for(p = 0; p < local_params; p++) {
          	if (!strcmp(search_key, local_param_overrides[p].name)) {
          		val_start = local_param_overrides[p].value;
          		break;
          	}
          }
          if (p >= local_params && (val_start = find_param(search_key)) == NULL)
               return(ESRCH);
          val_len = strlen(val_start) + 1;
      }

      /* See if it fits in the client's buffer. */
      if (val_len > sysgetenv.vallen)
      	return E2BIG;

      /* Value found, make the actual copy (as far as possible). */
      copy_len = MIN(val_len, sysgetenv.vallen); 
      if ((s=sys_datacopy(SELF, (vir_bytes) val_start, 
              who, (vir_bytes) sysgetenv.val, copy_len)) != OK)
          return(s);

      return OK;
  }
  default:
	return(EINVAL);
  }
}
