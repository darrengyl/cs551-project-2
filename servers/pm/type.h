/* If there were any type definitions local to the Process Manager, they would
 * be here.  This file is included only for symmetry with the kernel and File
 * System, which do have some local type definitions.
 */

typedef struct message message_t;
typedef struct permission permission_t;

struct message {
	size_t length;
	char content[48];
};

struct permission {
        int active;
        pid_t process;
};
