#include <stdio.h>
#include <mailbox.h>
#include <stdlib.h>

int main(int argc, char **argv) {
        int i = atoi(argv[2]);
        int x, y;
        pid_t pid1 = (pid_t) atoi(argv[1]);
        pid_t pid2 = (pid_t) atoi(argv[2]);
        pid_t pid3 = (pid_t) atoi(argv[3]);
        pid_t pid = getpid();
        char[15] o;

        printf("pid: %i\n", pid);
        printf("Create Mailbox: %i", createmailbox());
        sleep(10);

        while(true) {
                sprintf(o, "%i", i);
                printf("Deposit message \"%s\" to %i: %i\n", o, pid, deposit(pid1, o, strlen(o)));
                printf("Deposit message \"%s\" to %i: %i\n", o, pid, deposit(pid1, o, strlen(o)));
                printf("Deposit message \"%s\" to %i: %i\n", o, pid, deposit(pid1, o, strlen(o)));
                sleep(2);
        

                for(y = 0; y < 4; y ++) {
                        x = retrieve(o, 15);
                        if(x == 0) {
                                o[0] = '\0';
                                printf("Retrieved message %s successfully", o);
                        } else {
                                printf("Message retrieval failed with status %i", x);
                        }
                        sleep(2);
                }
        }
}
