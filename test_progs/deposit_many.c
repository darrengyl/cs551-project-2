#include <stdio.h>
#include <mailbox.h>
#include <stdlib.h>

int main(int argc, char **argv) {
        pid_t pid = (pid_t) atoi(argv[1]);
        int x;
        int y = atoi(argv[2]);
        for(x=0; x < y; x++) {
                printf("Message \"abcd\" deposited to %i: %i\n", pid, deposit(pid, "abcd", 4));
        }
}
